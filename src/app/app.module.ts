import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ProjectComponent } from './project/project.component';
import { AppComponent } from './app.component';
import { AlertModule, DatepickerModule, Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

import { routing }  from './app.routing';

@NgModule({
  declarations: [
    ProjectComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    AlertModule,
    DatepickerModule,
    Ng2BootstrapModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
