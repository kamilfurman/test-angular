import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import {ProjectComponent} from './project/project.component';


const appRoutes: Routes = [
  // ...loginRoutes
  { path: 'project', component: ProjectComponent }
];
export const appRoutingProviders: any[] = [
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
