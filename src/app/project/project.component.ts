import { Component, OnInit } from '@angular/core';

import { AlertModule, DatepickerModule, Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  public singleModel:string = '1';
  public radioModel:string = 'Middle';
  public checkModel:any = {left: false, middle: true, right: false};

  constructor() { }

  ngOnInit() {
  }

}

